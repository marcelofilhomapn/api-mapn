<?php

namespace App\Http\Controllers;

use Dingo\Api\Routing\Helpers;
use Illuminate\Routing\Controller;
use App\Models\Users;
use App\Models\Products;
use Illuminate\Http\Request;

class AppController extends BaseController
{
    function __construct()
    {
      $this->Users = new Users;
    }
    /**
    * @api {get} /user List
    * @apiGroup Users
    * @apiName List
    * @apiParamExample {json} Request-Example:
    *     {
    *       "facebook_id": "STRING",
    *       "facebook_token": "STRING"
    *     }
    * @apiSuccessExample {json} 200 OK
    * {
    *   "Users":
    *     {
    *       "_id": "57ae75bda697b2001046b09012390",
    *       "facebook_id": "57ae754da697b2000c0ba171",
    *       "facebook_token": "57ae754da697b2000c0ba171012909129010911",
    *       "updated_at": "2016-08-13 01:19:57",
    *       "created_at": "2016-08-13 01:19:57"
    *     }
    * }
    */
    function list( Request $request )
    {
      return $this->response->array( $this->query( $this->Users::select('_id', 'email', 'facebook_id'),  $request )->toArray() );
    }

    function getUser( $_id )
    {
      return $this->response->array( $this->Users->get( $_id ) );
    }
    
    public function query($model, $request = '')
    {
     return (!empty( $request->input() )) ? $this->queryExec($model, $request->input() )->get() : $model->take( (int) env('DB_LIMIT_RETURN') )->get() ;
    }

    function queryExec($model, $request)
    {
      foreach ($request as $key => $value)
        foreach (explode('AND', $value) as $index => $val)
          $model = $this->queryConditions( $model, $key, explode(',', $val)[0], explode(',', $val)[1] );
      return $model;
    }

    function queryConditions($model, $type = '' , $key, $value)
    {
      if ($type == 'where')
          $model = $model->where($key, $value);
      if ($type == 'orwhere')
          $model = $model->orWhere($key, $value);
      if ($type == 'order')
          $model = $model->orderBy($key, $value);
      return $model->take( ($type == 'take') ? (int) explode(',', $value)[0] : (int) env('DB_LIMIT_RETURN') );
    }
}
