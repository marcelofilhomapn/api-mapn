<?php

namespace App\Http\Controllers;

use Dingo\Api\Routing\Helpers;
use Illuminate\Routing\Controller;
use App\Models\Social;
use App\Http\Controllers\AppController;
use Illuminate\Http\Request;

class SocialController extends BaseController
{
    function __construct()
    {
      $this->Social = new Social;
      $this->AppController = new AppController;
    }

    function sharePhoto( Request $request )
    {
      return $this->response->array( $this->Social->sharePhoto( $request->input() )  );
    }
}
