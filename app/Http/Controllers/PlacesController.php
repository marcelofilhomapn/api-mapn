<?php

namespace App\Http\Controllers;

use Dingo\Api\Routing\Helpers;
use Illuminate\Routing\Controller;
use App\Models\Places;
use App\Http\Controllers\AppController;
use Illuminate\Http\Request;

class PlacesController extends BaseController
{
    function __construct()
    {
      $this->Places = new Places;
      $this->AppController = new AppController;
    }

    function insert( Request $request )
    {
      return $this->response->array( $this->Places->insert( $request->input() )  );
    }

    function favorite( Request $request )
    {
      return $this->response->array( $this->Places->favorite( $request->input() )  );
    }

    function get( $_id )
    {
      return $this->response->array( $this->Places->get( $_id )  );
    }

    function update(  Request $request , $_id)
    {
      return $this->response->array( $this->Places->edit( $request->input(), $_id )  );
    }

    function getByUser( $_id, $user)
    {
      return $this->response->array( $this->Places->getByUser( $_id, $user )  );
    }

    function listByUser(Request $request,  $user)
    {
      return $this->response->array( $this->Places->listByUser( $this->AppController->query($this->Places::select('_id', 'name', 'points', 'state', 'city', 'img', 'location') , $request)->toArray(),  $user ) );
    }

    function listByUserByDistance(Request $request, $user_id, $lat, $lgn, $distance, $limit)
    {
      return $this->response->array( $this->Places->listByUserByDistance( $user_id, $lat, $lgn, $distance, $limit) );
    }

    function list(Request $request)
    {
      return $this->response->array( $this->AppController->query($this->Places::select('_id', 'name', 'points', 'state', 'city', 'img', 'location') , $request)->toArray() );
    }
}
