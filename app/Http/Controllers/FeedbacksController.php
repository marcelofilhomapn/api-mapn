<?php

namespace App\Http\Controllers;

use Dingo\Api\Routing\Helpers;
use Illuminate\Routing\Controller;
use App\Models\Feedbacks;
use App\Http\Controllers\AppController;
use Illuminate\Http\Request;

class FeedbacksController extends BaseController
{
    function __construct()
    {
      $this->Feedbacks = new Feedbacks;
      $this->AppController = new AppController;
    }

    function insert( Request $request )
    {
      return $this->response->array( $this->Feedbacks->insert( $request->input() )  );
    }

    function getByUser( $_id, $user)
    {
      return $this->response->array( $this->Feedbacks->getByUser( $_id, $user )  );
    }

    function list(Request $request)
    {
      return $this->response->array( $this->AppController->query($this->Feedbacks, $request)->toArray() );
    }
}
