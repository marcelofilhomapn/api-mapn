<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Validator;
use App\Models\ValidatorModel;

class Feedbacks extends Eloquent
{
  protected $collection = 'Feedbacks';
  protected $ValidatorModel;
  protected static $unguarded = true;

  public function validateInsertFeedbacks( array $inputs )
  {
    $validator = Validator::make(
      $inputs,
    [
      'place_id' => 'required|string|exists:Places,_id',
      'user_id' => 'required|string|exists:Users,_id',
      'name' => 'required|string',
      'text'     => 'required|string',
      'pointing'     => 'string',
    ], ValidatorModel::$validatorMessage );
    if ($validator->fails()) throw new \Dingo\Api\Exception\StoreResourceFailedException('Parametros incorretos', array_values(array_filter($validator->errors()->toArray())));
    return false;
  }

  public function insert( array $inputs )
  {
    $this->validateInsertFeedbacks( $inputs );
    return $this::create( $inputs );
  }
}
