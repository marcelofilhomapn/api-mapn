<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Validator;
use App\Models\ValidatorModel;

class CheckIns extends Eloquent
{
  protected $collection = 'CheckIns';
  protected $ValidatorModel;
  protected static $unguarded = true;

  public function validateInsertCheckIns( array $inputs )
  {
    $validator = Validator::make(
      $inputs,
    [
      'checkIn' => 'required|string|exists:Places,_id',
      'user_id' => 'required|string|exists:Users,_id',
      'point'     => 'required|string',
    ], ValidatorModel::$validatorMessage );
    if ($validator->fails()) throw new \Dingo\Api\Exception\StoreResourceFailedException('Parametros incorretos', array_values(array_filter($validator->errors()->toArray())));
    return false;
  }

  public function insert( array $inputs )
  {
    $this->validateInsertCheckIns( $inputs );
    $points = $this::select('point')->where('status', 'active')->where('user_id', $inputs['user_id'])->where('checkIn', $inputs['checkIn'])->get()->toArray();
    if( (array_sum( array_column($points, 'point') ) + $inputs['point']) > 10) $this::where('status', 'active')->where('user_id', $inputs['user_id'])->where('checkIn', $inputs['checkIn'])->update(['status' => 'inactive']);
    return $this::create( array_merge( $inputs, ['status' => 'active']));
  }
}
