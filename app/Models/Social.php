<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Validator;
use App\Models\CheckIns;
use App\Models\Feedbacks;
use App\Models\Items;
use App\Models\Places;
use App\Models\PlacesFavorites;
use App\Models\UserItems;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Contracts\Filesystem\Filesystem;
use anlutro\cURL\cURL;

class Social extends Eloquent
{
  protected $collection = 'Social';

  public function validateSharePhoto( array $inputs )
  {
    $validator = Validator::make(
      $inputs,
    [
      'user_id'       => 'required|string|exists:Users,_id',
      'text'          => 'string',
      'photo'         => 'string',
      'social'        => 'array',
      'checkIn'       => 'string|exists:Places,_id',
    ], ValidatorModel::$validatorMessage );
    if ($validator->fails()) throw new \Dingo\Api\Exception\StoreResourceFailedException('Parâmetros incorreto', array_values(array_filter($validator->errors()->toArray())));

    return false;
  }

  public function sharePhoto( $inputs )
  {
    $point = 0;
    $this->validateSharePhoto( $inputs );
    $User = Users::where('_id', $inputs['user_id'])->first();

    $fileName =  $inputs['user_id'].'_'.date("Y-m-d H:i:s");
    $OutputFile =  "share/$fileName";
    $s3 = \Storage::disk('s3');
    $s3->put($OutputFile, file_get_contents( $inputs['photo'] ), 'public');
    $inputs['photo'] = \Storage::disk('s3')->url($OutputFile);

    if( !empty( $inputs['facebook'] ) AND $User->facebook_token )
    {
        $response = (new cURL)->post("https://graph.facebook.com/v2.6/me/photos?publish_actions=true&access_token=$User->facebook_token", ['message' => $inputs['text'], 'url' => $inputs['photo']] );
        if($response->info['http_code'] >= 400) return Response::json(['message' => json_decode($response->body)->error->message], 400);      
        $point = $point + 2;
    }

    CheckIns::create( array_merge( $inputs, ['status' => 'active', 'point' => $point ]));

    return ["success" => true, 'image' => \Storage::disk('s3')->url($OutputFile)];
  }
}
