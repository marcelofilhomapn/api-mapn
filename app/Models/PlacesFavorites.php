<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Validator;
use App\Models\ValidatorModel;

class PlacesFavorites extends Eloquent
{
  protected $collection = 'PlacesFavorites';
  protected $ValidatorModel;
  protected static $unguarded = true;

  public function deletePlacesFavorite( $_id )
  {
    return $this::where( '_id', $_id )->delete();
  }
}
