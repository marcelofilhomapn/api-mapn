<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Validator;
use App\Models\CheckIns;
use App\Models\Feedbacks;
use App\Models\Items;
use App\Models\Places;
use App\Models\PlacesFavorites;
use App\Models\UserItems;

class Users extends Eloquent
{
  protected $collection = 'Users';
  protected $hidden = ['password', 'code'];
  protected $ValidatorModel;
  protected static $unguarded = true;

  public function validateRegister( array $inputs )
  {
    $validator = Validator::make(
      $inputs,
    [
      'email'         => 'required|email|unique:Users,email',
      'name'          => 'required|string',
      'password'      => 'required|string|min:4',
    ], ValidatorModel::$validatorMessage );
    if ($validator->fails()) throw new \Dingo\Api\Exception\StoreResourceFailedException('Parâmetros incorreto', array_values(array_filter($validator->errors()->toArray())));

    return false;
  }


  public function validateLogin( array $inputs )
  {
    $validator = Validator::make(
      $inputs,
    [
      'email'        => 'required|email',
      'password'     => 'required|string|min:4',
    ], ValidatorModel::$validatorMessage );
    if ($validator->fails()) throw new \Dingo\Api\Exception\StoreResourceFailedException('Parâmetros incorretos', array_values(array_filter($validator->errors()->toArray())));
    return false;
  }

  public function validateFacebook( array $inputs )
  {
    $validator = Validator::make(
      $inputs,
    [
      'facebook_id'        => 'required|string',
      'facebook_token'     => 'string',
    ], ValidatorModel::$validatorMessage );
    if ($validator->fails()) throw new \Dingo\Api\Exception\StoreResourceFailedException('Parâmetros incorretos', array_values(array_filter($validator->errors()->toArray())));
    return false;
  }

  public function validateRegisterActive( array $inputs )
  {
    $validator = Validator::make(
      $inputs,
    [
      'code' => 'required|string|exists:Users,code'
    ]);
    if ($validator->fails()) throw new \Dingo\Api\Exception\StoreResourceFailedException('Could not active new user', array_values(array_filter($validator->errors()->toArray())));
    return false;
  }

  public function insert( array $inputs )
  {
    if(!empty($inputs['password'])) $inputs['password'] = md5($inputs['password']);
    if(empty($inputs['active'])) $inputs['active'] = 'true';
    return $this::create( $inputs );
  }

  public function facebook( $inputs )
  {
    $this->validateFacebook($inputs);
    $check = $this->where('facebook_id', $inputs['facebook_id'])->first();
    return (!empty($check)) ? $this->facebookUpdate($inputs) : $this::create( (array) $inputs)->toArray() ;
  }

  public function facebookUpdate( $inputs )
  {
    $this::where( 'facebook_id', $inputs['facebook_id'] )->update( $inputs );
    return $this->where('facebook_id', $inputs['facebook_id'])->first()->toArray();
  }

  public function login( $inputs )
  {
    $this->validateLogin($inputs);
    $login = $this->where('email', $inputs['email'])->where('password', md5($inputs['password']))->first();
    if ( empty($login) or in_array($login->type, ['admin']) ) throw new \Dingo\Api\Exception\StoreResourceFailedException('Email ou senha incorreto', []);
    return $login->toArray() ;
  }

  public function loginAdmin( $inputs )
  {
    $this->validateLogin($inputs);
    $login = $this->where('email', $inputs['email'])->where('password', md5($inputs['password']))->first();
    if ( empty($login) or !in_array($login->type, ['admin']) ) throw new \Dingo\Api\Exception\StoreResourceFailedException('Email ou senha incorreto', []);
    return $login->toArray() ;
  }

  public function register( $inputs )
  {
    $this->validateRegister($inputs);
    return $this->insert( (array) $inputs)->toArray() ;
  }

  public function updateUserValidate( array $inputs )
  {
    $validator = Validator::make(
      $inputs,
    [
      'description'  => 'string',
      'local'        => 'array',
      'expert'       => 'array',
      'expert.*'     => 'in:'.env('PROJECT_SERVICE_TYPE'),

    ], ValidatorModel::$validatorMessage );
    if ($validator->fails()) throw new \Dingo\Api\Exception\StoreResourceFailedException('Professional não pode ser atualizado', array_values(array_filter($validator->errors()->toArray())));
    return false;
  }

  public function edit( $inputs, $_id )
  {
    return $this::where( '_id', $_id )->update( $inputs );
  }

  public function get( $_id, $Return = [] )
  {
    $PlacesFavorites = PlacesFavorites::select('place_id', 'created_at')->where('user_id', $_id)->get()->toArray();
    $CheckIns = CheckIns::select('_id', 'checkIn', 'point', 'status', 'created_at')->where('user_id', $_id )->get()->toArray();

    $Places = Places::select('_id', 'name', 'img', 'state', 'city')->whereIn('_id',   array_unique( array_merge( array_column( $CheckIns, 'checkIn'), array_column($PlacesFavorites, 'place_id') ) ) )->get()->keyBy('_id')->toArray();
    
    $UserItems = UserItems::where('user_id', $_id)->get()->toArray();
    $Items = Items::select('*')->whereIn('_id', array_unique( array_column( $UserItems, 'item_id') ) )->get()->keyBy('_id')->toArray();

    foreach ($PlacesFavorites as $key => $PlacesFavorite) 
    {
      if( empty( $Places[$PlacesFavorite['place_id']] ) ) continue;
      $Return['PlacesFavorites'][] = array_merge( $PlacesFavorite, ['Place' => $Places[$PlacesFavorite['place_id']]] );
    }

    foreach ($CheckIns as $key => $CheckIn) 
    {
      if( empty( $Places[$CheckIn['checkIn']] ) ) continue;
      $Return['CheckIns'][] = array_merge( $CheckIn, ['Place' => $Places[$CheckIn['checkIn']]] );
    }
    
    foreach ($UserItems as $key => $UserItem) 
    {
      if( empty( $Items[$UserItem['item_id']] ) ) continue;
      $Return['UserItems'][] = array_merge( $UserItem, ['Item' => $Items[$UserItem['item_id']]] );
    }
    return array_merge( 
      $this::where( '_id', $_id )->first()->toArray(),
      $Return
    );
  }
}
