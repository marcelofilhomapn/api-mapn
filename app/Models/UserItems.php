<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Validator;
use App\Models\CheckIns;
use App\Models\Items;
use App\Models\ValidatorModel;

class UserItems extends Eloquent
{
  protected $collection = 'UserItems';
  protected $ValidatorModel;
  protected static $unguarded = true;

  public function validateInsertUserItems( array $inputs )
  {
    $validator = Validator::make(
      $inputs,
    [
      'item_id' => 'required|string|exists:Items,_id',
      'user_id' => 'required|string|exists:Users,_id',
      'point'   => 'required|string',
    ], ValidatorModel::$validatorMessage );
    if ($validator->fails()) throw new \Dingo\Api\Exception\StoreResourceFailedException('Parametro incorreto', array_values(array_filter($validator->errors()->toArray())));
    return false;
  }

  public function insert( array $inputs )
  {
    $this->validateInsertUserItems( $inputs );
    $place = Items::where('_id',  $inputs['item_id'])->first()->place_id;
    $CheckIns = CheckIns::select('_id')->where('checkIn', $place)->where('status', 'active')->take( (int) $inputs['point'] )->get()->toArray();
    if( ((int)$inputs['point']) >= 10) 
    {
      UserItems::where('user_id', $inputs['user_id'] )->where('place_id', $inputs['place_id'] )->where('status', 'active' )->update(['status' => 'inactive']);
      CheckIns::whereIn('_id', array_column($CheckIns, '_id'))->update(['status' => 'inactive']);   
      $this::create( array_merge($inputs, ['status' => 'inactive']) );       
    } 
    else
    {
        $this::create( array_merge($inputs, ['status' => 'active']) );        
    }
    return [];
  }
  
  public function getActivesPointUserByPlace( $place,  $user)
  {
    $get = CheckIns::select('point', 'photo', 'status')->where('checkIn', $place)->where('user_id', $user )->get()->toArray();
    $photo = $actives = $total = 0;
    foreach ($get as $key => $value) {
      if( $value["status"] == 'active' ) $actives++;
      if( !empty($value["photo"]) ) $photo++;
      $total++;
    }
    return [
      'actives' => $actives,
      'photos' => $photo,
      'total' => $total,
    ];
  }
}
